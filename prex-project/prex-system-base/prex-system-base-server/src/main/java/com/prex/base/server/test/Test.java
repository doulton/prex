package com.prex.base.server.test;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.prex.base.api.entity.SysMenu;
import com.prex.base.server.PrexSystemBaseServerApplication;
import com.prex.base.server.service.ISysMenuService;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @Classname Test
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-11 16:14
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PrexSystemBaseServerApplication.class)
public class Test {

    @Autowired
    private ISysMenuService menuService;


    @org.junit.Test
    public void test() {

        List<SysMenu> list = menuService.list();
        JSONArray menujsonArray = new JSONArray();
        this.getPermissionJsonArray(menujsonArray, list, null);

        System.out.println(menujsonArray);
    }


    /**
     * 获取菜单JSON数组
     *
     * @param jsonArray
     * @param metaList
     * @param parentJson
     */
    private void getPermissionJsonArray(JSONArray jsonArray, List<SysMenu> metaList, JSONObject parentJson) {
        for (SysMenu permission : metaList) {
            if (permission.getType() == null) {
                continue;
            }
            Integer tempPid = permission.getParentId();
            JSONObject json = getPermissionJsonObject(permission);
            if (json == null) {
                continue;
            }

            if (parentJson == null && tempPid.equals(0)) {
                jsonArray.add(json);
                getPermissionJsonArray(jsonArray, metaList, json);
            } else if (parentJson != null && tempPid != 0 && tempPid.equals(Integer.parseInt(parentJson.getString("id")))) {
                // 类型( 0：一级菜单 1：子菜单 2：按钮 )
                if (permission.getType().equals(1) || permission.getType().equals(0)) {
                    if (parentJson.containsKey("children")) {
                        parentJson.getJSONArray("children").add(json);
                    } else {
                        JSONArray children = new JSONArray();
                        children.add(json);
                        parentJson.put("children", children);
                    }
                    getPermissionJsonArray(jsonArray, metaList, json);
                }
            }

        }
    }


    private JSONObject getPermissionJsonObject(SysMenu permission) {
        JSONObject json = new JSONObject();
        // 类型(0：一级菜单 1：子菜单 2：按钮)
        if (permission.getType().equals(2)) {
            //json.put("action", permission.getPerms());
            //json.put("type", permission.getPermsType());
            //json.put("describe", permission.getName());
            return null;
        } else if (permission.getType().equals(0) || permission.getType().equals(1)) {
            json.put("id", permission.getMenuId());
//            if (permission.isRoute()) {
//                json.put("route", "1");// 表示生成路由
//            } else {
//                json.put("route", "0");// 表示不生成路由
//            }

            if (isWWWHttpUrl(permission.getPath())) {
                json.put("path", permission.getPath());
            } else {
                json.put("path", permission.getPath());
            }

            // 重要规则：路由name (通过URL生成路由name,路由name供前端开发，页面跳转使用)
            if (StrUtil.isNotEmpty(permission.getComponent())) {
                json.put("name", urlToRouteName(permission.getName()));
            }

            // 是否隐藏路由，默认都是显示的
//            if (permission.getHidden()) {
//                json.put("hidden", true);
//            }
//            // 聚合路由
//            if (permission.getAlwaysShow()) {
//                json.put("alwaysShow", true);
//            }

            if (permission.getParentId().equals(0)) {
                //一级目录需要加斜杠，不然访问 会跳转404页面
                json.put("component", StrUtil.isEmpty(permission.getComponent()) ? "Layout" : permission.getComponent());
            } else if (!StrUtil.isEmpty(permission.getComponent())) {
                json.put("component", permission.getComponent());
            }

            JSONObject meta = new JSONObject();
            // 由用户设置是否缓存页面 用布尔值
            if (permission.getKeepAlive()) {
                meta.put("keepAlive", true);
            } else {
                meta.put("keepAlive", false);
            }

            meta.put("title", permission.getName());
            if (permission.getParentId() == 0) {
                // 一级菜单跳转地址
                json.put("redirect", permission.getRedirect());
                if (StrUtil.isNotEmpty(permission.getIcon())) {
                    meta.put("icon", permission.getIcon());
                }
            } else {
                if (StrUtil.isNotEmpty(permission.getIcon())) {
                    meta.put("icon", permission.getIcon());
                }
            }
            if (isWWWHttpUrl(permission.getPath())) {
                meta.put("url", permission.getPath());
            }
            json.put("meta", meta);
        }

        return json;
    }

    /**
     * 判断是否外网URL 例如： http://localhost:8080/jeecg-boot/swagger-ui.html#/ 支持特殊格式： {{
     * window._CONFIG['domianURL'] }}/druid/ {{ JS代码片段 }}，前台解析会自动执行JS代码片段
     *
     * @return
     */
    private boolean isWWWHttpUrl(String url) {
        if (url != null && (url.startsWith("http://") || url.startsWith("https://") || url.startsWith("{{"))) {
            return true;
        }
        return false;
    }

    /**
     * 通过URL生成路由name 举例： URL = /isystem/role RouteName = role
     *
     * @return
     */
    private String urlToRouteName(String url) {
        if (StrUtil.isNotEmpty(url)) {
            int i = url.lastIndexOf("/");
            return url.substring(i + 1);
        } else {
            return null;
        }
    }
}
